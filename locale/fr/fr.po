# Translation of registerQuick in French (France)
# This file is distributed under the same license as the registerQuick package.
msgid ""
msgstr ""
"PO-Revision-Date: 2018-06-29 13:19:33+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: GlotPress/2.3.1\n"
"Language: fr\n"
"Project-Id-Version: registerQuick\n"

#: views/registerForm.php:61 views/registerForm.php:88
msgid "Continue"
msgstr "Continuer"

#: views/registerForm.php:55
msgid "Fields marked with an asterisk (%s) are mandatory."
msgstr "Les champs marqués d‘un astérisque (%s) sont obligatoires."

#: registerQuick.php:355
msgid "%s cannot be left empty"
msgstr "%s ne peut être laisser vide"

#: registerQuick.php:349
msgid "You must enter a valid email. Please try again."
msgstr "Vous devez entre une adresse électronique valide. veuillez réessayer."

#: registerQuick.php:347
msgid "The email you used is not valid. Please try again."
msgstr "L‘adresse électronique est invalide. Veuillez réessayer."

#: registerQuick.php:336
msgid "Your answer to the security question was not correct - please try again."
msgstr "Votre réponse à la question de sécurité est incorrecte - veuillez réessayer."

#: registerQuick.php:282
msgid "This email address is already registered, entering in survey is only allowed with token."
msgstr "L‘adresse électronique est déjà enregistrée, entrer dans le questionnaire nécessite le code d‘invitation."

#: registerQuick.php:279
msgid "This email address is already registered but the email adress was bounced."
msgstr "L‘adresse électronique est déjà enregistrée, mais elle est indiquée en erreur."

#: registerQuick.php:277
msgid "This email address cannot be used because it was opted out of this survey."
msgstr "L‘adresse électronique que vous avez entré ne peut pas être utilisée, elle est indiqué comme ne voulant plus recevoir de nouveaux messages."

#: registerQuick.php:274
msgid "The email address you have entered is already registered and the survey has been completed."
msgstr "L‘adresse électronique que vous avez entré est déjà enregistrée et le questionnaire à était complété."

#: registerQuick.php:248
msgid "Registering to : %s"
msgstr "Enregistrement à : %s"

#: registerQuick.php:84
msgid "You must activate twigExtendByPlugins plugin"
msgstr "Vous devez activer le plugin twigExtendByPlugins"

#: registerQuick.php:81
msgid "You must download twigExtendByPlugins plugin"
msgstr "Vous devez télécharger le plugin twigExtendPlugins"

#: registerQuick.php:76
msgid "Only for LimeSurvey 3.0.0 and up version"
msgstr "Seulement pour LimeSurvey version 3.0 et supérieure"

#: registerQuick.php:135
msgid "If email exist : disable reloading survey without token. Warning: enabling reload of survey only with the e-mail address can cause privacy issue. The message will be sent if the email address exists."
msgstr "Si l‘adresse courriel existe : empêcher le rechargement des réponses sans le code d‘invitation. Attention : permettre le rechargement des réponses avec seulement l‘adresse de courriel peut entrainer des problèmes sur les données privées. Les message d‘enregistrement sera envoyé si l‘adresse de courriel existe."

#: registerQuick.php:134
msgid "Privacy of response"
msgstr "Confidentialité de la réponse"

#: registerQuick.php:124
msgid "Reload previous response."
msgstr "Recharger la réponse précédente."

#: registerQuick.php:122
msgid "Existing Email"
msgstr "Adresse existante"

#: registerQuick.php:146
msgid "Show the token form."
msgstr "Ajouter le formulaire de jeton."

#: registerQuick.php:141
msgid "If user put an email address, the register message will be sent."
msgstr "Si l‘utilisateur inique une adresse courriel, le message sera envoyé."

#: registerQuick.php:140
msgid "Send the email."
msgstr "Envoyer le message courriel."

#: registerQuick.php:128
msgid "Create a new token each time."
msgstr "Créer un nouveau jeton à chaque fois."

#: registerQuick.php:125
msgid "Create a new one if already completed"
msgstr "Créer un nouveau jeton seulement si marqué terminé"

#: registerQuick.php:116
msgid "Validate like LimeSurvey core (default)"
msgstr "Valider comme le fait LimeSurvey (par défaut)"

#: registerQuick.php:113
msgid "Hide and don't use it"
msgstr "Masque et ne pas l'utiliser"

#: registerQuick.php:112
msgid "Shown but allow empty"
msgstr "Montre mais permet vide"

#: registerQuick.php:110
msgid "Email settings"
msgstr "Paramètres de courriel"

#: registerQuick.php:105
msgid "Use quick registering"
msgstr "Utiliser l'enregistrement rapide"